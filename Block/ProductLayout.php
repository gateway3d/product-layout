<?php

namespace Gateway3D\ProductLayout\Block;

use Magento\Framework\View\Element\Template;

/**
 * Main returns form block
 */
class ProductLayout extends Template
{	
	protected $_registry;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, \Magento\Framework\Registry $registry,array $data = [])
    {
        $this->_registry = $registry;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        
    }

    public function getCurrentProduct()
    {        
        return $this->_registry->registry('current_product');
    }    

}
