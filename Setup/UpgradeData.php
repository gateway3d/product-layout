<?php

namespace Gateway3D\ProductLayout\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
	/**
	 * EAV setup factory
	 *
	 * @var EavSetupFactory
	 */
	private $eavSetupFactory;
	private $storeRepository;

	/**
	 * Init
	 *
	 * @param EavSetupFactory $eavSetupFactory
	 */
	public function __construct(
		EavSetupFactory $eavSetupFactory,
		\Magento\Store\Model\StoreRepository $storeRepository
	)
	{
		$this->eavSetupFactory = $eavSetupFactory;
		$this->storeRepository = $storeRepository;
	}

	/**
	 * {@inheritdoc}
	 */
	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		if (version_compare($context->getVersion(), '2.3.0.2') < 0) {
			/** @var EavSetup $eavSetup */
			$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);				

			$groupId = (int)$eavSetup->getAttributeGroupByCode(
                \Magento\Catalog\Model\Product::ENTITY,
                'Default',
                'gateway3d-product-layout',
                'attribute_group_id'
            );
			
			$eavSetup->updateAttributeGroup(
				\Magento\Catalog\Model\Product::ENTITY,
				'Default', 
				$groupId,  
				'attribute_group_name', 
				'Custom Gateway Product Layout'
			);
        }
	}
}
